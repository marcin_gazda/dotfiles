#!/bin/sh

ln -sf ${PWD}/bashrc ~/.bashrc

ln -sf ${PWD}/zsh/zshrc ~/.zshrc
ln -sf ${PWD}/zsh/p10k.zsh ~/.p10k.zsh

mkdir -p ~/.vim
ln -sf ${PWD}/vim/* ~/.vim

mkdir -p ~/.config/git
ln -sf ${PWD}/git/ignore ~/.config/git

ln -sf ${PWD}/tmux/tmux.conf ~/.tmux.conf

ln -sf ${PWD}/screen/screenrc ~/.screenrc

mkdir -p ~/.xmonad
ln -sf ${PWD}/xmonad/xmonad.hs ~/.xmonad/

mkdir -p ~/.config/xmobar
ln -sf ${PWD}/xmobar/xmobarrc ~/.config/xmobar
